# Description
Sample code of Google Map with HTML, CSS and AngularJS
For running the map you will have to get an [API key](https://developers.google.com/maps/documentation/javascript/get-api-key) from google

## Kickstart of AngularJS with Yeoman
This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
