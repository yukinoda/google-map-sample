'use strict';

/**
 * @ngdoc function
 * @name gmap3App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gmap3App
 */
angular.module('gmap3App')
  .controller('MainCtrl', ['$scope', '$log', '$timeout', 'uiGmapGoogleMapApi',
  				        function ($scope, $log, $timeout, uiGmapGoogleMapApi) {
    // Do stuff with your $scope.
    // Note: Some of the directives require at least something to be defined originally!
    // e.g. $scope.markers = []
    $scope.map = {
      center: { 
        latitude: 35.690841, 
        longitude: 139.699568
      },
      zoom: 16
    };
    
    //Add Marker Function
    function geo(la,lo) {
      return {
        id: 0,
        coords: {
          latitude: la, 
          longitude: lo
        },
        options: {
          draggable: true,
          animation: 2// 1: BOUNCE, 2: DROP
        },
        events: {
          dragend: function (marker, eventName, args) {
            $log.log('marker dragend');
            var lat = marker.getPosition().lat();
            var lon = marker.getPosition().lng();
            $log.log(lat);
            $log.log(lon);
          }
        }
      }      
    };
    
    $scope.marker = geo($scope.map.center.latitude, $scope.map.center.longitude);

    $scope.searchbox = { 
        template: 'searchbox.tpl.html',
        events: {
          places_changed: function (searchBox){
            var places = searchBox.getPlaces();
            $log.debug(places);

            if (places.length > 0){
              var place = places[0];
              //$log.debug(place.geometry.location.lat());
              //$log.debug(place.geometry.location.lng());
 
              $scope.marker = null;
              if (!$scope.marker){
                  setTimeout(function(){
                    $scope.marker = geo(place.geometry.location.lat(),place.geometry.location.lng());
                  }, 350);
                  $scope.map.center = {
                    latitude: place.geometry.location.lat(),
                    longitude: place.geometry.location.lng()
                  };
                return;
              };
              return;
            };
          }
        }
    }

    // uiGmapGoogleMapApi is a promise.
    // The "then" callback function provides the google.maps object.
    uiGmapGoogleMapApi.then(function(maps) {
      maps.visualRefresh = true;
    });
  }]);
