'use strict';

/**
 * @ngdoc function
 * @name gmap3App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the gmap3App
 */
angular.module('gmap3App')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
