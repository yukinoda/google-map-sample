'use strict';

/**
 * @ngdoc overview
 * @name gmap3App
 * @description
 * # gmap3App
 *
 * Main module of the application.
 */
angular
  .module('gmap3App', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'uiGmapgoogle-maps'

  ])
  .config(['$routeProvider', 'uiGmapGoogleMapApiProvider', function ($routeProvider, uiGmapGoogleMapApiProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });

    uiGmapGoogleMapApiProvider.configure({
      key: '', //You can get your API key here: https://developers.google.com/maps/documentation/javascript/get-api-key
      v: '3.25', //defaults to latest 3.X anyhow
      language: 'ja',
      region: 'JP',
      libraries: 'weather,geometry,visualization,places'
    });

  }]);